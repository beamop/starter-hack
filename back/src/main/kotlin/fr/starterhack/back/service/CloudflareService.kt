package fr.starterhack.back.service

import com.fasterxml.jackson.databind.ObjectMapper
import eu.roboflax.cloudflare.CloudflareAccess
import eu.roboflax.cloudflare.CloudflareRequest
import eu.roboflax.cloudflare.constants.Category
import eu.roboflax.cloudflare.objects.ResultInfo
import eu.roboflax.cloudflare.objects.dns.DNSRecord
import fr.starterhack.back.dao.ChallengeStateRepository
import fr.starterhack.back.model.ChallengeState
import fr.starterhack.back.model.State
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class CloudflareService(
        @Value("\${cloudflare.email}") private val cfEmail: String,
        @Value("\${cloudflare.token}") private val cfToken: String,
        @Value("\${cloudflare.zone.id}") private val zoneId: String,
        private val cfAccess: CloudflareAccess = CloudflareAccess(cfToken, cfEmail),
        @Autowired private val stateRepo: ChallengeStateRepository
) {

    fun getDnsRecords(): String {
        val response = CloudflareRequest(Category.LIST_DNS_RECORDS, cfAccess)
                .identifiers(zoneId)
                .queryString("per_page", 1000)
                .asObjectList(DNSRecord::class.java)

        return response.json.toString()
    }

    fun getDnsRecord(name: String): String {
        val response = CloudflareRequest(Category.LIST_DNS_RECORDS, cfAccess)
                .identifiers(zoneId)
                .queryString("name", name)
                .asObjectList(DNSRecord::class.java)

        return response.json.toString()
    }

    fun deleteUnusedDns() {
        val unusedDns: MutableList<String> = ArrayList()

        val dnsRecords: MutableList<DNSRecord>? = CloudflareRequest(Category.LIST_DNS_RECORDS, cfAccess)
                .identifiers(zoneId)
                .queryString("per_page", 1000)
                .asObjectList(DNSRecord::class.java)
                .`object`

        val intDns: MutableList<String> = ArrayList()
        dnsRecords?.forEach {
            val name = it.name.removeSuffix(".starterhack.fr")
            val int = name.toIntOrNull()
            if (int != null ) intDns.add(it.name)
        }


        intDns.forEach {
            val challenge = stateRepo.findByStateAndHost(State.STARTED, host = it)
            if (challenge == null) unusedDns.add(it)
        }

        return unusedDns.forEach{ it -> deleteDnsRecord(it) }
    }

    fun getSecurityLevel(): String {
        val response = CloudflareRequest(Category.SECURITY_LEVEL_SETTING, cfAccess)
                .identifiers(zoneId)
                .send()

        return response.json.toString()
    }

    fun createDnsRecord(ec2Host: String, fullSubName: String? = null, isProxied: Boolean): String {
        var subName: String

        subName = if (fullSubName == null) {
            (1000..50000).random().toString()
        } else {
            fullSubName
        }
        val response = CloudflareRequest(Category.CREATE_DNS_RECORD, cfAccess)
                .identifiers(zoneId)
                .body("type", "A")
                .body("name", subName)
                .body("content", ec2Host)
                .body("ttl", 1)
                .body("proxied", isProxied)
                .asObject(DNSRecord::class.java)
        return response.json.toString()
    }

    fun deleteDnsRecord(name: String): Boolean {
        val recordStr = getDnsRecord(name)
        val recordObj = ObjectMapper().readTree(recordStr)
        if (recordObj.size() > 0) {
            val recordId = recordObj.get("result").get(0).get("id").asText()
            return try {
                val response = CloudflareRequest(Category.DELETE_DNS_RECORD, cfAccess)
                        .identifiers(zoneId, recordId)
                        .send()
                response.isSuccessful

            } catch (e: Exception) {
                println(e)
                false
            }

        }

        return false
    }

    fun updateSecurityLevel(securityLevel: String): String {
        val response = CloudflareRequest(Category.CHANGE_SECURITY_LEVEL_SETTING, cfAccess)
                .identifiers(zoneId)
                .body("value", securityLevel)
                .asObject(ResultInfo::class.java)

        return response.json.toString()
    }

}