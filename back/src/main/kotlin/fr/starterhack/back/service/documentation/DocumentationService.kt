package fr.starterhack.back.service.documentation

import com.amazonaws.services.s3.model.S3ObjectSummary
import fr.starterhack.back.dao.DocumentationRepository
import fr.starterhack.back.model.Documentation
import fr.starterhack.back.service.AwsS3Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Primary
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.multipart.MultipartFile
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import javax.transaction.Transactional

@Service
@Primary
class DocumentationService(@Autowired var documentationRepository: DocumentationRepository,
                           @Autowired var s3Service: AwsS3Service,
                           @Value("\${S3_STORAGE_BUCKET}") private val s3Bucket: String,
                           @Value("\${AWS_REGION}") private val s3Region: String): IDocumentationService {

    @Transactional
    override fun getDocumentations(): MutableIterable<Documentation> {
        fetchS3Docs()
        deleteUnusedFile()
        return documentationRepository.findAll()
    }

    @Transactional
    override fun getDocumentationById(id: Int): Documentation {
        return documentationRepository.findById(id).get()
    }

    @Transactional
    override fun deleteDocumentationByFilename(fileName: String) {
        documentationRepository.deleteByName(fileName)
        s3Service.deleteFileInBucket("docs", fileName)
    }

    @Transactional
    override fun updateDocumentation(documentation: Documentation){
        var docs = getDocumentations()
        var oldDoc = documentation.id?.let { getDocumentationById(it) }
        if(docs.find { it.name == documentation.name } == null) {
            s3Service.updateFileInBucket("docs", oldDoc?.name!!, documentation.name!!)
            oldDoc?.name = documentation.name
            oldDoc?.url = "https://${s3Bucket}.s3.${s3Region}.amazonaws.com/docs/" + documentation.name
            documentationRepository.save(oldDoc!!)
        }
        else throw HttpClientErrorException(HttpStatus.BAD_REQUEST, "Name already used.")
    }

    @Transactional
    override fun insertDocumentation(documentation: Documentation, file: MultipartFile?): Documentation? {
        if(!documentation.name!!.endsWith(".pdf")) {
            val ext = file?.contentType?.split('/')!![1]
            documentation.name += ".$ext"
        }
        if(file != null)
            documentation.url = "https://${s3Bucket}.s3.${s3Region}.amazonaws.com/docs/" +
                    URLEncoder.encode(documentation.name, StandardCharsets.UTF_8.toString())
        return documentationRepository.save(documentation)
    }

    @Transactional
    override fun fetchS3Docs() {
        var docs = documentationRepository.findAll() as List<Documentation>
        var s3Docs = s3Service.getFilesInBucket("docs")
        s3Docs?.map { s3Doc -> s3Doc
            var fileName = s3Doc.key.split("/")[1]
            var fileUrl = "https://${s3Bucket}.s3.${s3Region}.amazonaws.com/docs/" + URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString())
            if( fileName != "" && docs.find { it.name == fileName } == null) {
                val doc = Documentation( name = fileName, url = fileUrl )
                documentationRepository.save(doc)
            }
        }
    }

    @Transactional
    override fun deleteUnusedFile() {
        var allDocs = documentationRepository.findAll() as List<Documentation>
        var s3Docs = s3Service.getFilesInBucket("docs")
        allDocs.forEach { doc: Documentation -> if(doc.url!!.startsWith("https://${s3Bucket}.s3.${s3Region}.amazonaws.com/docs/")
                && s3Docs?.find { s3Doc: S3ObjectSummary -> s3Doc.key == "docs/${doc.name}" } == null) {
                documentationRepository.delete(doc)
            }
        }

    }
}