package fr.starterhack.back.service

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.model.News
import okhttp3.OkHttpClient
import okhttp3.Request
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class NewsService {

    val rss2jsonApi: String = "https://api.rss2json.com/v1/api.json?rss_url="
    val theHackerNewsFeed: String = "https://feeds.feedburner.com/TheHackersNews?format=xml"

    fun getNews(): News? {
        val client = OkHttpClient()
        val objectMapper = ObjectMapper()

        val request = Request.Builder()
            .url(rss2jsonApi + theHackerNewsFeed)
            .build()
        val response = client.newCall(request).execute().body!!.string()

        return objectMapper.readValue(response, News::class.java)
    }

}