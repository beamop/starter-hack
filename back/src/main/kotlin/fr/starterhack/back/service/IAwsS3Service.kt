package fr.starterhack.back.service

import com.amazonaws.services.s3.model.ListObjectsV2Request
import com.amazonaws.services.s3.model.ListObjectsV2Result
import com.amazonaws.services.s3.model.S3ObjectSummary
import org.springframework.web.multipart.MultipartFile
import java.io.File

interface IAwsS3Service {
    fun uploadFile(multipartFile: MultipartFile, location: String): String?
    fun convertMultiPartFileToFile(multipartFile: MultipartFile): File
    fun uploadFileToS3Bucket(location: String, file: File): String
    fun getFilesInBucket(location: String): MutableList<S3ObjectSummary>?
    fun getFile(location: String, fileName: String): S3ObjectSummary?
    fun deleteFileInBucket(location: String, fileName: String)
    fun updateFileInBucket(location: String, oldName: String, newName: String)
}