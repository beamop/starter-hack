package fr.starterhack.back.service.documentation

import fr.starterhack.back.dao.DocumentationRepository
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Documentation
import org.springframework.web.multipart.MultipartFile

interface IDocumentationService {

    fun getDocumentations() : MutableIterable<Documentation>
    fun getDocumentationById(id: Int) : Documentation
    fun deleteDocumentationByFilename(fileName: String)
    fun updateDocumentation(documentation: Documentation)
    fun insertDocumentation(documentation: Documentation, file: MultipartFile?): Documentation?
    fun fetchS3Docs()
    fun deleteUnusedFile()
}