package fr.starterhack.back.service

import com.amazonaws.services.cloudformation.model.CreateStackRequest
import com.amazonaws.services.cloudformation.model.Parameter
import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.dao.*
import fr.starterhack.back.model.*
import fr.starterhack.back.model.response.DNSRecordResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.lang.Thread.sleep
import java.util.*
import javax.transaction.Transactional
import kotlin.collections.ArrayList

@Service
class ChallengeService(
        @Value("\${reverse_proxy.ip}") private val reverseProxyIp: String,
        @Value("\${S3_STORAGE_BUCKET}") private val s3Bucket: String,
        @Value("\${AWS_REGION}") private val s3Region: String,
        @Autowired private val awsService: AwsService,
        @Autowired private val cloudflareService: CloudflareService,
        @Autowired private val challengeRepo: ChallengeRepository,
        @Autowired private val stateRepo: ChallengeStateRepository,
        @Autowired private val commentRepo: CommentRepository
) {

    @Transactional
    fun getChallenge(challengeId: Int): Optional<Challenge> {
        val challenge = challengeRepo.findById(challengeId)
        challenge.get().flag
        return challenge
    }

    @Transactional
    fun getChallenges(): MutableIterable<Challenge> {
        var challenges = challengeRepo.findAll()
        challenges.map { it.flag = "" }
        return challenges
    }

    @Transactional
    fun getChallengesByCategory(categoryName: String): MutableList<Challenge>? {
        var challenges = challengeRepo.findByCategoryName(categoryName)
        challenges?.map { it.flag = ""}
        return challenges
    }

    @Transactional
    fun runChallenge(challenge: Challenge, user: Person): ChallengeState {
        if (!challenge.script.isNullOrEmpty()) {
            val request: CreateStackRequest = createStackRequest(challenge, user.id as Int)
            awsService.createCloudFormationStack(request)
        }
        return initChallengeState(challenge, user)
    }

    @Transactional
    fun initChallengeState(challenge: Challenge, user: Person): ChallengeState {

        val stackName = "ch${challenge.id}-user${user.id}";

        if (!challenge.script.isNullOrEmpty()) {

            while(awsService.getCloudFormationStack(stackName)
                            !!.stacks[0]
                            .stackStatus == "CREATE_IN_PROGRESS") {
                awsService.getCloudFormationStack(stackName)
                sleep(1000)
            }
        }

        val osUsername = awsService.getStackOutput(stackName, "EC2User")
        val osPassword = awsService.getStackOutput(stackName, "EC2Mdp")

        var host: String;
        if (challenge.host.isNullOrEmpty()) {
            val ip = awsService.getStackOutput(stackName, "StackVPC")
            host = setChallengeHost(challenge, ip!!)
        } else {
            host = challenge.host!!
        }

        val challengeState = ChallengeState(
                challenge = challenge,
                user = user,
                state = State.STARTED,
                osPassword = osPassword,
                osUsername = osUsername,
                host = host)

        stateRepo.save(challengeState)
        runTimer(challengeState)

        return challengeState
    }

    @Transactional
    fun setChallengeHost(challenge: Challenge, ip: String): String {
        val objectMapper = ObjectMapper()
        if(!challenge.host.isNullOrEmpty()) return challenge.host!!
        if (challenge.category.name.toLowerCase().startsWith("web")) {
            val cloudflareResponse = objectMapper.readValue(
                    cloudflareService.createDnsRecord(ec2Host = reverseProxyIp, isProxied = true),
                    DNSRecordResponse::class.java)
            return cloudflareResponse.result!!.name
        }
        return ip
    }

    @Transactional
    fun createStackRequest(challenge: Challenge, userId: Int): CreateStackRequest {
        var req = CreateStackRequest()
                .withStackName("ch${challenge.id}-user${userId}")
                .withTemplateURL(challenge.script)
        if (!challenge.amiId.isNullOrEmpty()) {
            val amiId: Parameter = Parameter()
                    .withParameterKey("ImageId")
                    .withParameterValue(challenge.amiId)
            req.withParameters(amiId)
            }
       return req
    }

    @Transactional
    fun endChallenge(stateId: Int, resolved: Boolean): ChallengeState {
        val state = stateRepo.findById(stateId).get()
        if (!state.challenge.script.isNullOrEmpty())
            this.awsService.deleteCloudFormationStack("ch${state.challenge.id}-user${state.user.id}")
        if (state.challenge.host?.endsWith("starterhack.fr")!! && state.challenge.host.isNullOrEmpty())
            this.cloudflareService.deleteDnsRecord(state.host)

        if (resolved) {
            if (!this.hasResolvedChallenge(state.challenge.id as Int, state.user.id as Int)) {
                state.user.addPoints(state.challenge.points)
            }
            state.state = State.RESOLVED
            if(state.challenge.mustBeDelete)
                deleteChallenge(state.challenge.id!!)
            if(state.challenge.challengeUpdate != null){
                updateChallenge(state.challenge.challengeUpdate!!)
            }
        } else {
            state.state = State.FAILED
        }
        return stateRepo.save(state)
    }

    @Transactional
    fun runTimer(challengeState: ChallengeState) {
        val time = Timer()
                time.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                if ((challengeState.challenge.timer - (Date().time - challengeState.startDate.time)/1000).toInt() == 0) {
                    endChallenge(challengeState.id!!, false)
                    time.cancel()
                }
            }
        }, 1000, 1000)
    }

    @Transactional
    fun addChallenge(challenge: Challenge) : Challenge {
        challenge.timer = if(challenge.timer == 0) 3600 else challenge.timer
        challengeRepo.save(challenge)
        return challenge
    }

    @Transactional
    fun getValidationsCount(challengeId: Int):Int {
        val validations = this.stateRepo.findByChallengeIdAndState(challengeId, State.RESOLVED)
                .groupBy { ch -> ch.user }
        return validations.size
    }

    @Transactional
    fun isChallengeAvailable(challengeId: Int, userId: Int): Boolean {
        return this.awsService.getCloudFormationStack("ch${challengeId}-user${userId}") == null
                || this.stateRepo.findByUserIdAndChallengeIdAndState(userId, challengeId, State.STARTED).isEmpty();
    }

    @Transactional
    fun getResolvedChallenges(userId: Int): MutableList<Challenge> {
        var challenges: MutableList<Challenge> = ArrayList<Challenge>();
        this.stateRepo.findByUserIdAndState(userId, State.RESOLVED)
                .forEach { state -> challenges.add(state.challenge)}

        return challenges
    }

    @Transactional
    fun hasResolvedChallenge(userId: Int, challengeId: Int): Boolean {
        return this.stateRepo.findByUserIdAndChallengeIdAndState(userId, challengeId, State.RESOLVED).isNotEmpty()
    }

    @Transactional
    fun getStartedChallenges(userId: Int): MutableList<ChallengeState> {
        return this.stateRepo.findByUserIdAndState(userId, State.STARTED)
    }

    @Transactional
    fun getStartedChallenge(userId: Int, challengeId: Int): ChallengeState? {
        var states = this.stateRepo.findByUserIdAndChallengeIdAndState(userId, challengeId, State.STARTED)
        return if (states.isNotEmpty()) states[0] else null
    }

    @Transactional
    fun isChallengeRunning(challengeId: Int): Boolean {
        var states = stateRepo.findAll()
        return stateRepo.findByChallengeIdAndState(challengeId, State.STARTED).isNotEmpty()
    }

    fun updateChallenge(challenge: Challenge): Challenge {
        var challengeToUpdate: Challenge = challengeRepo.findById(challenge.id!!).orElse(null)

        if(isChallengeRunning(challenge.id!!)) {
            challengeToUpdate.challengeUpdate = challenge
        }
        else {
            challengeToUpdate = challenge
            challengeToUpdate.challengeUpdate = null
        }
        return challengeRepo.save(challengeToUpdate)
    }

    @Transactional
    fun deleteChallenge(challengeId: Int){
        var challengeToDelete: Challenge = challengeRepo.findById(challengeId).orElse(null)
        var tries = this.stateRepo.findByChallengeId(challengeId)

        if(isChallengeRunning(challengeId)){
            challengeToDelete.mustBeDelete = true
        }
        else {
            if (tries.size > 0) challengeToDelete.enabled = false
            else challengeRepo.delete(challengeToDelete)
        }
    }

    @Transactional
    fun commentChallenge(comment: Comment){
        var commentSave: Comment = comment
       if(comment.challenge != null && comment.user != null){

           var existingComment: Comment? =  commentRepo.findByChallengeIdAndUserId(comment.challenge.id!!, comment.user.id!!)

           if(existingComment != null){
               existingComment.comment = comment.comment
               existingComment.rating = comment.rating
               commentSave = existingComment
           }
           commentRepo.save(commentSave)
       }
    }

    @Transactional
    fun getComments(challengeId: Int) : MutableList<Comment>{
        return commentRepo.findByChallengeId(challengeId)!!
    }

    @Transactional
    fun deleteComment(challengeId: Int, userId: Int){
        var comment = commentRepo.findByChallengeIdAndUserId(challengeId, userId)
        if(comment != null)
            commentRepo.delete(comment)
    }

    fun searchChallenge(search: String): MutableIterable<Challenge>? {
        return challengeRepo!!.findChallengeByNameIgnoreCaseLike("%$search%")
    }
}