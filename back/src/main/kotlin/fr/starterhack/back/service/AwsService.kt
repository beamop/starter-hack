package fr.starterhack.back.service

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.cloudformation.AmazonCloudFormation
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient
import com.amazonaws.services.cloudformation.model.*
import com.amazonaws.services.cloudformation.model.Stack
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.DescribeInstancesRequest
import com.amazonaws.services.ec2.model.DescribeInstancesResult
import com.amazonaws.services.ec2.model.InstanceType
import com.amazonaws.services.ec2.model.RunInstancesRequest
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import java.util.*


@Service
@Primary
class  AwsService(
        @Value("\${AWS_ACCESS_KEY_ID}") private val accessKey: String,
        @Value("\${AWS_SECRET_ACCESS_KEY}") private val secretKey: String,
        awsCreds: BasicAWSCredentials = BasicAWSCredentials(accessKey, secretKey),
        @Value("\${AWS_REGION}") private val region: String,

        val ec2Client: AmazonEC2 = AmazonEC2Client.builder()
                .withRegion(region)
                .withCredentials(AWSStaticCredentialsProvider(awsCreds))
                .build(),
        val cloudFormationClient: AmazonCloudFormation = AmazonCloudFormationClient.builder()
                .withRegion(region)
                .withCredentials(AWSStaticCredentialsProvider(awsCreds))
                .build()
) {

    fun getEc2Instances(): DescribeInstancesResult {
       return ec2Client.describeInstances();
    }

    fun getEc2Instance(instanceId: String): DescribeInstancesResult {
        val request = DescribeInstancesRequest().withInstanceIds(instanceId);
        return ec2Client.describeInstances(request);
    }

    fun getCloudFormationStacks(): DescribeStacksResult {
        return cloudFormationClient.describeStacks();
    }

    fun getCloudFormationStack(stackName: String): DescribeStacksResult? {
        val request = DescribeStacksRequest()
                .withStackName(stackName)
        return try {
            cloudFormationClient.describeStacks(request);
        } catch (e: Exception) {
            null
        }
    }

    fun getStackOutput(stackName: String, outputName: String): String? {
        val stack = getCloudFormationStack(stackName)?.stacks?.get(0);
        return stack?.outputs?.find { output -> output.outputKey == outputName }?.outputValue
    }

    fun createCloudFormationStack(request: CreateStackRequest?): CreateStackResult {
        return cloudFormationClient.createStack(request)
    }

    fun deleteCloudFormationStack(stackName: String): DeleteStackResult {
        val request = DeleteStackRequest()
                .withStackName(stackName)
        return cloudFormationClient.deleteStack(request)
    }
}