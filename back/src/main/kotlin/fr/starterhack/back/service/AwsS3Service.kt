package fr.starterhack.back.service

import com.amazonaws.AmazonServiceException
import com.amazonaws.SdkClientException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.cloudformation.AmazonCloudFormation
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.time.LocalDateTime


@Service
class AwsS3Service(
        @Value("\${AWS_ACCESS_KEY_ID}") private val accessKey: String,
        @Value("\${AWS_SECRET_ACCESS_KEY}") private val secretKey: String,
        awsCreds: BasicAWSCredentials = BasicAWSCredentials(accessKey, secretKey),
        @Value("\${AWS_REGION}") private val region: String,
        private val amazonS3: AmazonS3 = AmazonS3Client.builder()
                .withRegion(region)
                .withCredentials(AWSStaticCredentialsProvider(awsCreds))
                .build()
) : IAwsS3Service {
    @Value("\${S3_STORAGE_BUCKET}")
    private val bucketName: String? = null

    @Value("\${AWS_REGION}")
    private val s3Region: String? = null

    @Async
    override fun uploadFile(multipartFile: MultipartFile, location: String): String? {
        try {
            val file: File = convertMultiPartFileToFile(multipartFile)
            val fileName = uploadFileToS3Bucket(location, file)
            file.delete()
            return "https://$bucketName.s3.$s3Region.amazonaws.com/$location/$fileName"

        } catch (ex: AmazonServiceException) {
            return null
        }
    }

    override fun convertMultiPartFileToFile(multipartFile: MultipartFile): File {
        val file = File(multipartFile.originalFilename)
        FileOutputStream(file).use { outputStream -> outputStream.write(multipartFile.bytes) }
        return file
    }

    override fun uploadFileToS3Bucket(location: String, file: File): String {
        val uniqueFileName: String =
                when (location) {
                    "scripts" -> location + '/' + file.name
                    "docs" -> {
                        location + '/' + file.nameWithoutExtension + ".pdf"
                    }
                    else -> location + '/' + LocalDateTime.now().toString() + "_" + file.name
                }

        val putObjectRequest = PutObjectRequest(bucketName, uniqueFileName, file)
        amazonS3.putObject(putObjectRequest)
        return uniqueFileName
    }

    override fun getFilesInBucket(location: String): MutableList<S3ObjectSummary>? {
        val listObjectsRequest = ListObjectsV2Request()
                .withBucketName(bucketName)
                .withPrefix(location)
                .withStartAfter(location)
        return amazonS3!!.listObjectsV2(listObjectsRequest).objectSummaries
    }

    override fun getFile(location: String, fileName: String): S3ObjectSummary? {
        return getFilesInBucket(location)?.find { it.key == "$location/$fileName" }
    }

    override fun deleteFileInBucket(location: String, fileName: String) {
        try {
        amazonS3!!.deleteObject(DeleteObjectRequest(bucketName, "$location/$fileName"))
        } catch (e: AmazonServiceException) {
            e.printStackTrace();
        } catch (e: SdkClientException) {
            e.printStackTrace();
        }
    }

    override fun updateFileInBucket(location: String, oldName: String, newName: String) {
                try {
                    println(oldName)
                    val copyObjRequest = CopyObjectRequest(bucketName, "$location/$oldName", bucketName, "$location/$newName")
                    amazonS3.copyObject(copyObjRequest)

                    deleteFileInBucket("docs", oldName)

        } catch (e: AmazonServiceException) {
            e.printStackTrace();
        } catch (e: SdkClientException) {
            e.printStackTrace();
        }
    }
}