package fr.starterhack.back.service.user

import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.model.update.UpdateUser

interface IUserService {

    var userRepo: UserRepository?

    fun insertUser(username: String,email: String, password: String): Person?
    fun insertAdminAsAdmin(user: Person): Person
    fun insertSuperAdmin(user: Person): Person
    fun selectUser(email: String, password: String): Person?
    fun selectUserByEmail(email: String): Person?
    fun selectUserByUsername(username: String): Person?
    fun selectUserById(id: Int): Person?
    fun setUserRole(username: String, role: UserRole)
    fun selectAllPerson(): MutableIterable<Person>?
    fun updateUser(username: String, user: UpdateUser): Person?
    fun updateUser(user: Person, isUser: Boolean)
    fun updateUserPhoto(username: String, photoUrl: String?): Person?
    fun deleteUserByEmail(email: String)
    fun deleteUserAsAdmin(id: Int)
    fun selectUsersOrderedByPoints(): List<Person>
    fun searchUser(search: String): MutableIterable<Person?>
}