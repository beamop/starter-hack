package fr.starterhack.back.dao

import fr.starterhack.back.model.ChallengeCategory
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ChallengeCategoryRepository : CrudRepository<ChallengeCategory, Int> {
    fun findByName(name: String): ChallengeRepository?
    abstract fun findById(id: Int?): Optional<ChallengeCategory>
}