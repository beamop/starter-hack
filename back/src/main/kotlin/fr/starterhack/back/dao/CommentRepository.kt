package fr.starterhack.back.dao

import fr.starterhack.back.model.ChallengeState
import fr.starterhack.back.model.Comment
import org.springframework.data.repository.CrudRepository

interface CommentRepository : CrudRepository<Comment, Int> {
    fun findByChallengeId(challengeId: Int): MutableList<Comment>?
    fun findByChallengeIdAndUserId(challengeId: Int, userId: Int): Comment?
}