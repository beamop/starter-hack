package fr.starterhack.back.dao

import fr.starterhack.back.model.Proposal
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProposalRepository : CrudRepository<Proposal, Int> {
    fun findByUserId(id: Int): Proposal
}