package fr.starterhack.back.security

import fr.starterhack.back.model.Person
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.UserDetails

class CustomUserDetails(val user: Person) : UserDetails{


    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(user.role.toString())
    }

    override fun isEnabled(): Boolean {
        return true;
    }

    override fun getUsername(): String {
        return user.username
    }
    fun getId(): String {
        return user.id.toString()
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true;
    }

    override fun getPassword(): String {
        return user.password
    }

    override fun isAccountNonExpired(): Boolean {
        return true;
    }

    override fun isAccountNonLocked(): Boolean {
        return true;
    }


}