package fr.starterhack.back.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private val jwtTokenFilter: JwtTokenFilter? = null

    override fun configure(httpSecurity: HttpSecurity) {
        http.headers().frameOptions().sameOrigin();
        httpSecurity
                .csrf().disable()
                .cors()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/account/login").permitAll()
                .antMatchers("/account/register").permitAll()
                .antMatchers("/ws/**").permitAll()
                .antMatchers("/cloudflare/**").hasAnyAuthority("ADMIN", "SUPERADMIN")
                .antMatchers("/aws/**").hasAnyAuthority("ADMIN", "SUPERADMIN")
                .antMatchers("/healthcheck").permitAll()
                .anyRequest().authenticated()

        httpSecurity.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

}



