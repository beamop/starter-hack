package fr.starterhack.back.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Item(
        var title: String? = "",
        var pubDate: String? = "",
        var link: String? = "",
        var guid: String? = "",
        var author: String? = "",
        var thumbnail: String? = "",
        var description: String? = "",
        var content: String? = ""
)