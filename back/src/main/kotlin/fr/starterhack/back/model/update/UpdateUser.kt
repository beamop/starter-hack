package fr.starterhack.back.model.update

import com.fasterxml.jackson.annotation.JsonProperty
import fr.starterhack.back.model.UserRole
import org.springframework.web.multipart.MultipartFile
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class UpdateUser(
@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

@Size(min = 5, max = 50, message
= "Le mot de passe doit contenir entre 5 et 50 caractères")
var password: String? = null,
var passwordConfirm: String? = null,

@get: NotBlank(message = "Email obligatoire")
@get: Email(message = "Email invalide")
var email: String,
var school: String? = null,
var company: String? = null,
var description: String? = null
)