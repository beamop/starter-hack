package fr.starterhack.back.model

data class News(
        var status: String? = "",
        var feed: Feed? = null,
        var items: List<Item>? = listOf()
)