package fr.starterhack.back.model

import java.util.*
import javax.persistence.*

@Entity
data class ChallengeState(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Int? = null,
        var state: State = State.STARTED,
        @ManyToOne var challenge: Challenge = Challenge(),
        var osUsername: String? = null,
        var osPassword: String? = null,
        var host: String = "",
        var startDate: Date = Date(),
        var endDate: Date? = null,
        @OneToOne var user: Person = Person()
)

enum class State { RESOLVED, FAILED, STARTED }
