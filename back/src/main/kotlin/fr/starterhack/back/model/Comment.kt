package fr.starterhack.back.model

import java.util.*
import javax.persistence.*

@Entity
data class Comment (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,
    @OneToOne
    var challenge: Challenge = Challenge(),
    var rating: String? = null,
    var comment: String? = null,
    @OneToOne
    var user: Person = Person()
)