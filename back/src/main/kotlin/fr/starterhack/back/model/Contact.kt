package fr.starterhack.back.model

import javax.persistence.*

@Entity
data class Contact (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Int? = null,
        @ManyToOne var adder: Person = Person(),
        @ManyToOne var added: Person = Person()
)