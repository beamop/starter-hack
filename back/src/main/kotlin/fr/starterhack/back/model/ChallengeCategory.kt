package fr.starterhack.back.model

import javax.persistence.*

@Entity
data class ChallengeCategory (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Int? = null,
        @Column(unique=true) var name: String = "",
        var icon: String? = null
)