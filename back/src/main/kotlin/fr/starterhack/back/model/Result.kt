package fr.starterhack.back.model


import com.fasterxml.jackson.annotation.JsonProperty

data class Result(
    @JsonProperty("content")
    val content: String,
    @JsonProperty("created_on")
    val createdOn: String,
    @JsonProperty("id")
    val id: String,
    @JsonProperty("locked")
    val locked: Boolean,
    @JsonProperty("meta")
    val meta: Meta,
    @JsonProperty("modified_on")
    val modifiedOn: String,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("proxiable")
    val proxiable: Boolean,
    @JsonProperty("proxied")
    val proxied: Boolean,
    @JsonProperty("ttl")
    val ttl: Int,
    @JsonProperty("type")
    val type: String,
    @JsonProperty("zone_id")
    val zoneId: String,
    @JsonProperty("zone_name")
    val zoneName: String
)