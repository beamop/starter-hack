package fr.starterhack.back.model.response

data class Response (
    var success: Boolean,
    var message: String = ""
)