package fr.starterhack.back.model

import javax.persistence.*

@Entity
data class Proposal(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = 0,
        var title: String = "",
        var category: String = "",
        @Lob
        var description: String = "",
        var level: Int = 0,
        var status: ProposalStatus? = ProposalStatus.NEW,
        @ManyToOne
        var user: Person? = null
)

enum class ProposalStatus { NEW, ACCEPTED, PROCESSED }