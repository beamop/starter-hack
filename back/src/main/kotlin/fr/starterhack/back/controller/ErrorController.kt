package fr.starterhack.back.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.messaging.support.ErrorMessage
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*


@ControllerAdvice
class ErrorController {


    var stackTrace = true


    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    fun processAllError(ex: MethodArgumentNotValidException): ResponseEntity<MutableList<ObjectError?>?> {
        val details: MutableList<ObjectError?> = ArrayList()
        for (error in ex.bindingResult.allErrors) {
            details.add(error)
        }


        return ResponseEntity(details, HttpStatus.UNAUTHORIZED)
    }
}