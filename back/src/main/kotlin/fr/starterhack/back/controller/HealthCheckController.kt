package fr.starterhack.back.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("healthcheck")
class HealthCheckController {
    @GetMapping
    fun check(): String {
        return "OK ${Date()}"
    }
}