package fr.starterhack.back.controller

import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.model.response.Response
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import fr.starterhack.back.service.user.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/users")
class UserController(
        @Autowired private val jwtUserService: JwtUserDetailsService,
        @Autowired var jwtTokenService: JwtTokenService? = null,
        @Autowired private val template: SimpMessagingTemplate
) {

    fun getCurrentPerson(): Person {
        val authentication: Authentication = SecurityContextHolder.getContext().authentication
        val userDetails: CustomUserDetails = authentication.principal as CustomUserDetails
        userDetails.user.password = "null"
        return userDetails.user
    }

    @Autowired
    var userService: IUserService? = null

    @GetMapping
    fun getUsers(): MutableIterable<Person>? {
        return userService!!.selectAllPerson()
    }

    @GetMapping("{id}")
    fun getUserById(@PathVariable id: Int): Person? {
        return userService!!.selectUserById(id)
    }

    @GetMapping("/rank")
    fun getUsersOrderedByPoints(): List<Person> {
        return userService!!.selectUsersOrderedByPoints()
    }

    @PutMapping()
    fun updateUser(principal: Principal, @RequestBody user: Person): ResponseEntity<Any> {
        val reqUser = getCurrentPerson()
        if (reqUser.id != user.id && reqUser.role == UserRole.USER) {
            return ResponseEntity(Response(false, "You can't update this account"), HttpStatus.FORBIDDEN)
        }

        template.convertAndSend("/topic/user", user)

        userService!!.updateUser(user, reqUser.id==user.id)

        return if (user.username == principal.name) {
            var userDetails: CustomUserDetails? = jwtUserService.loadUserByEmail(user.email) as CustomUserDetails?
            userDetails?.user?.token = jwtTokenService?.generateToken(userDetails!!)
            ResponseEntity(userDetails?.user, HttpStatus.OK)
        } else ResponseEntity(Response(true, "The user has been updated"), HttpStatus.OK)
    }

    @DeleteMapping("{id}")
    fun deleteUserById(@PathVariable id: Int): ResponseEntity<Any> {
        var reqUser = getCurrentPerson()
        if (reqUser.id != id && reqUser.role == UserRole.USER) {
            return ResponseEntity(Response(false, "You can't delete this account"), HttpStatus.FORBIDDEN)
        }
        return ResponseEntity(userService!!.deleteUserAsAdmin(id), HttpStatus.OK)
    }

    @PostMapping("/admin")
    @PreAuthorize("hasAuthority('SUPERADMIN')")
    fun insertAdmin(@RequestBody user: Person): Person {
        return userService!!.insertAdminAsAdmin(user)
    }

}