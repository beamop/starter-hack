package fr.starterhack.back.controller

import fr.starterhack.back.model.News
import fr.starterhack.back.service.NewsService
import okhttp3.ResponseBody
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("news")
class NewsController(
        @Autowired val newsService: NewsService
) {

    @GetMapping
    fun getNews(): News? {
        return this.newsService.getNews()
    }

}