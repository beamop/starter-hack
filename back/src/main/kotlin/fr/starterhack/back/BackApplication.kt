package fr.starterhack.back

import fr.starterhack.back.service.user.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class BackApplication()

fun main(args: Array<String>) {
	runApplication<BackApplication>(*args)
}
