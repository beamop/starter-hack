package fr.starterhack.back.controller.account

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.AuthViewModel
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AccountPostLoginTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val testsUtils: TestsUtils
) {

    val mapper = ObjectMapper()

    @BeforeEach
    fun insertPerson() {
        testsUtils.insertUser()
    }

    fun login(email: String, password: String?=null): ResultActions {
        val user = AuthViewModel(
                email = email
        )
        if(password != null) user.password = password
        val body = mapper.writeValueAsString(user)
        return mvc.perform(MockMvcRequestBuilders.post("/account/login")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
    }


    @Test()
    @DisplayName("Login with email")
    fun login_with_email_should_return_user_with_token() {
        login(testsUtils.email, testsUtils.password )
                .andDo { res -> run {
                    @Suppress("NAME_SHADOWING")
                    val user = mapper.readValue(res.response.contentAsString, Person::class.java)
                    Assertions.assertNotNull(user.token)
                    Assertions.assertEquals(user.username, testsUtils.username)
                    }
                }
    }

    @Test()
    @DisplayName("Login with username")
    fun login_with_username_should_return_user_with_token() {
        login(testsUtils.username, testsUtils.password )
                .andDo { res -> run {
                    @Suppress("NAME_SHADOWING")
                    val user = mapper.readValue(res.response.contentAsString,Person::class.java)
                    Assertions.assertNotNull(user.token)
                    Assertions.assertEquals(user.username, testsUtils.username)
                    }
                }
    }

    @Test()
    @DisplayName("Try to login with incorrect email")
    fun login_with_incorrect_email_should_return_403() {
        login("incorrect@starterhack.fr", testsUtils.password)
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @Test()
    @DisplayName("Try to login with incorrect password")
    fun login_with_incorrect_password_should_return_403() {
        login(testsUtils.email, "incorrectPassword")
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)
    }

    @Test()
    @DisplayName("Try to login without password")
    fun login_without_password_should_return_403() {
        login(testsUtils.email)
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)
    }
}