package fr.starterhack.back.controller.user

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Person
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

class UserPutTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val userRepo: UserRepository,
        @Autowired private val testsUtils: TestsUtils) {

    val mapper = ObjectMapper()

    @BeforeEach
    fun insertPerson() {
        testsUtils.insertUser()
    }

    fun updateUser(id: Int, username: String, email: String, jwt: String? = null): ResultActions {
        var token: String? = jwt ?: testsUtils.createToken(testsUtils.username)

        val user = Person(
                id = testsUtils.id,
                username = "TestUser2",
                email = testsUtils.email
        )
        val body = mapper.writeValueAsString(user)
        return  mvc.perform(MockMvcRequestBuilders.put("/users")
                .header("Authorization", "Bearer $token")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
    }

    fun updateUserWithoutAuth(id: Int, username: String, email: String): ResultActions {

        val user = Person(
                id = testsUtils.id,
                username = "TestUser2",
                email = testsUtils.email
        )
        val body = mapper.writeValueAsString(user)
        return  mvc.perform(MockMvcRequestBuilders.put("/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
    }

    @Test()
    @DisplayName("Try to update user with an other user account")
    fun update_user_with_another_user_account_should_return_403() {
        val user = testsUtils.insertUser(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr"))
        val token = testsUtils.createToken(user.username)
        val username = "TestUser2"
        this.updateUser(2, username, testsUtils.password, token)
                .andExpect(MockMvcResultMatchers.status().isForbidden)
        Assertions.assertNull(userRepo.findByUsername(username))
    }

    @Test()
    @DisplayName("Update user with an other admin account")
    fun update_user_with_another_admin_account() {
        val user = testsUtils.insertAdmin(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr"))
        val token = testsUtils.createToken(user.username)
        val username = "TestUser2"
        this.updateUser(2, username, testsUtils.password, token)
                .andExpect(MockMvcResultMatchers.status().isOk)
        Assertions.assertNotNull(userRepo.findByUsername(username))
    }

    @Test()
    @DisplayName("Update user with an other super admin account")
    fun update_user_with_another_super_admin_account() {
        val user = testsUtils.insertSuperAdmin(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr"))
        val token = testsUtils.createToken(user.username)
        val username = "TestUser2"
        this.updateUser(testsUtils.id, username, testsUtils.password, token)
                .andExpect(MockMvcResultMatchers.status().isOk)
        Assertions.assertNotNull(userRepo.findByUsername(username))
    }

    @Test()
    @DisplayName("Try to update user without authentication")
    fun update_user_without_authentication_should_return_403() {
        val username = "TestUser2"
        this.updateUserWithoutAuth(testsUtils.id, username, testsUtils.password)
                .andExpect(MockMvcResultMatchers.status().isForbidden)
        Assertions.assertNull(userRepo.findByUsername(username))
    }
}