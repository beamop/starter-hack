package fr.starterhack.back.controller.user

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.Person
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

class UserGetIdTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val testsUtils: TestsUtils,
        @Autowired private val userRepo: UserRepository) {

    val mapper = ObjectMapper()

    @BeforeEach
    fun insertPerson() {
        testsUtils.insertUser()
    }

    @Test()
    @DisplayName("Get the user")
    fun get_user() {
        val token = testsUtils.createToken(testsUtils.username)

        var result: MvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/${testsUtils.id}")
                .header("Authorization", "Bearer $token"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn();


        val user = mapper.readValue(result.response.contentAsString, Person::class.java)
        Assertions.assertTrue(user.id == 2)
        Assertions.assertTrue(userRepo.findById(testsUtils.id).get().username == user.username)
    }

    @Test()
    @DisplayName("Try to get an user without authentication")
    fun get_user_without_authentication_return_403() {

        var result: MvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/${testsUtils.id}"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test()
    @DisplayName("Try to get an user with wrong id")
    fun get_user_with_wrong_id_return_404() {

        var result: MvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/10"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

}