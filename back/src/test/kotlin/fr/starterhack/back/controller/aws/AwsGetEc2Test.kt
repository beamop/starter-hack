package fr.starterhack.back.controller.aws

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AwsGetEc2Test(
        @Autowired private val mvc: MockMvc,
        @Autowired private val testsUtils: TestsUtils
) {

    private val mapper = ObjectMapper()

    fun getInstances(jwt: String? = null): ResultActions {
        var token: String? = jwt ?: testsUtils.createToken(testsUtils.username)
        return mvc.perform(MockMvcRequestBuilders.get("/aws/ec2")
                .header("Authorization", "Bearer $token"))
                .andDo(MockMvcResultHandlers.print())
    }

    fun getInstancesWithoutAuth(): ResultActions {
        return mvc.perform(MockMvcRequestBuilders.get("/aws/ec2"))
                .andDo(MockMvcResultHandlers.print())
    }

    @Test()
    @DisplayName("Try to get ec2 instances without token")
    fun get_ec2_instances_without_jwt_should_return_403() {
        var result: MvcResult = getInstancesWithoutAuth()
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test()
    @DisplayName("Get EC2 instances with admin account")
    fun get_ec2_instances_with_admin_account() {
        val admin = testsUtils.insertAdmin()
        val token = testsUtils.createToken(admin.username)

        var result: MvcResult = getInstances(token)
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.sdkHttpMetadata.httpStatusCode").value(200))
        .andReturn()
    }

    @Test()
    @DisplayName("Get EC2 instances with super admin account")
    fun get_ec2_instances_with_super_admin_account() {
        val admin = testsUtils.insertSuperAdmin()
        val token = testsUtils.createToken(admin.username)

        var result: MvcResult = getInstances(token)
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.sdkHttpMetadata.httpStatusCode").value(200))
                .andReturn()
    }

    @Test()
    @DisplayName("Try to return EC2 instances with user an account")
    fun get_ec2_instances_with_user_account_should_return_403() {
        val admin = testsUtils.insertUser()
        val token = testsUtils.createToken(admin.username)

        var result: MvcResult = getInstances()
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn();
    }
}

