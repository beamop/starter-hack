package fr.starterhack.back.controller.account

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.dao.UserRepository
import fr.starterhack.back.model.AuthViewModel
import fr.starterhack.back.model.Person
import fr.starterhack.back.model.UserRole
import fr.starterhack.back.security.CustomUserDetails
import fr.starterhack.back.service.jwt.JwtTokenService
import fr.starterhack.back.service.jwt.JwtUserDetailsService
import fr.starterhack.back.service.user.UserService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AccountGetUserTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val testsUtils: TestsUtils
) {

    private val mapper = ObjectMapper()


    @Test()
    @DisplayName("Try to get user without token")
    fun get_user_without_jwt_should_return_403() {
        testsUtils.insertUser()
        mvc.perform(MockMvcRequestBuilders.get("/account/user"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @Test()
    @DisplayName("Return a user")
    fun get_user_should_return_user() {
        testsUtils.insertUser()
        val token = testsUtils.createToken(testsUtils.username)

        var result: MvcResult = mvc.perform(MockMvcRequestBuilders.get("/account/user")
                .header("Authorization", "Bearer $token"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn();


        val user = mapper.readValue(result.response.contentAsString, Person::class.java)
        Assertions.assertNotNull(user)
    }
}