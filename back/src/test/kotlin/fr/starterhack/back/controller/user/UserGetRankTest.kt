package fr.starterhack.back.controller.user

import com.fasterxml.jackson.databind.ObjectMapper
import fr.starterhack.back.TestsUtils
import fr.starterhack.back.model.Person
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

class UserGetRankTest(
        @Autowired private val mvc: MockMvc,
        @Autowired private val testsUtils: TestsUtils) {

    val mapper = ObjectMapper()

    @BeforeEach
    fun insertPerson() {
        testsUtils.insertUser()
    }

    @Test()
    @DisplayName("Get the users rank")
    fun get_users_tank() {
        val user = testsUtils.insertAdmin(Person(username = "TestUser1", password="testtest", email = "testmail@starterhack.fr", points = 150))
        val token = testsUtils.createToken(testsUtils.username)

        var result: MvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/rank")
                .header("Authorization", "Bearer $token"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        val users = mapper.readValue(result.response.contentAsString, Array<Person>::class.java)

        Assertions.assertTrue(users[0].id == user.id)
        Assertions.assertTrue(users[0].points > users[1].points)
    }

    @Test()
    @DisplayName("Try to get the users rank without authentication")
    fun get_the_users_rank_without_authentication_return_403() {
        var result: MvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/rank"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

}