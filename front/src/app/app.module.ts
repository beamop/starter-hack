import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { RegisterComponent } from './components/register/register.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { UserManageComponent } from './components/user/user-manage/user-manage.component';
import { ChallengeAddComponent } from './components/challenge/challenge-add/challenge-add.component';
import { ChallengeService } from './services/challenge.service';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ChallengeDetailsComponent } from './components/challenge/challenge-details/challenge-details.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { ChallengeListComponent } from './components/challenge/challenge-list/challenge-list.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ProposalsComponent } from './components/proposals/proposals.component';
import { ProposalsDetailsComponent } from './components/proposals/proposals-details/proposals-details.component';
import { ProposalsPipe } from './pipes/proposals.pipe';
import { ProposalsAddComponent } from './components/proposals/proposals-add/proposals-add.component';
import { DocumentationListComponent } from './components/documentation/documentation-list/documentation-list.component';
import { DocumentationAddComponent } from './components/documentation/documentation-add/documentation-add.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SweetAlertService } from './services/sweet-alert.service';
import { DocumentationAddFormComponent } from './components/documentation/documentation-add-form/documentation-add-form.component';
import { NgxBootstrapModule } from './ngx-bootstrap/ngx-bootstrap.module';
import { SanitizePipe } from './pipes/sanitize.pipe';
import { BarRatingModule } from "ngx-bar-rating";
import { RankListComponent } from './components/rank/rank-list/rank-list.component';
import { SearchComponent } from './components/search/search.component';
import { ServerStatusComponent } from './components/server-status/server-status.component';
import { CloudflareComponent } from './components/cloudflare/cloudflare.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    UserListComponent,
    UserManageComponent,
    ChallengeListComponent,
    ChallengeDetailsComponent,
    ChallengeAddComponent,
    UserProfileComponent,
    ProposalsComponent,
    ProposalsDetailsComponent,
    ProposalsPipe,
    ProposalsAddComponent,
    DocumentationListComponent,
    DocumentationAddComponent,
    DocumentationAddFormComponent,
    SanitizePipe,
    RankListComponent,
    SearchComponent,
    ServerStatusComponent,
    CloudflareComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    SweetAlert2Module.forRoot(),
    CKEditorModule,
    PdfViewerModule,
    NgxBootstrapModule,
    BarRatingModule
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: JwtInterceptor, 
      multi: true 
    },
    ChallengeService,
    SweetAlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }