import { Feed } from './feed.model'
import { Item } from './item.model'

export class News {
    status: string
    feed: Feed
    items: Item[]
}
