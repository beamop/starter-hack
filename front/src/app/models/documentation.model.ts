export class Documentation {
    id?: number
    name: string
    url: string
}
