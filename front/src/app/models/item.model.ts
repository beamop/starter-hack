import { SafeHtml } from '@angular/platform-browser'

export class Item {
    title: string
    pubDate: string
    link: string
    guid: string
    author: string
    thumbnail: string
    description: string
    content: string
    safeContent: SafeHtml
}