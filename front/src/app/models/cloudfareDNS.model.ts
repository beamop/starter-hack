export class CloudflareDNS {
    id: number
    name: string
    content: string
    proxied: boolean
}