export class User {
    id?: number
    username: string
    email: string
    role: string
    school?: string
    company?: string
    photo?: string
    description?: string
    points: number
    token: string
}