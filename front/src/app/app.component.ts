import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebSocketService } from './services/websocket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public router: Router, private websocketService: WebSocketService) {
   }

  ngOnInit() {
    this.websocketService.connect()
  }

  displayBody() {
      return !this.router.url.startsWith('/login') && !this.router.url.startsWith('/register')
  }
}
