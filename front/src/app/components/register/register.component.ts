import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup
  get username(){return this.form.get("username")}
  get email(){return this.form.get("email")}
  get password(){return this.form.get("password")}
  get passwordrep(){return this.form.get("passwordrep")}

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router
             ) { }

  ngOnInit() {
    this.initForm()
  }

  initForm() {
    this.form = this.formBuilder.group({
      username: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
      passwordrep: ["", Validators.required]
    })
  }

  onSubmit() {
    if(this.form.invalid) return false
    this.authService.register(this.username.value, this.email.value, this.password.value, this.passwordrep.value)
    .subscribe(
      (user: User) => {
        this.router.navigate(['/login']);
      },
      err => {
        console.error(err.error.message)
      }
    )
  }

}
