import { Component, OnInit } from '@angular/core';
import { Proposal } from 'src/app/models/proposal.model';
import { ProposalService } from 'src/app/services/proposal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-proposals',
  templateUrl: './proposals.component.html',
  styleUrls: ['./proposals.component.scss']
})
export class ProposalsComponent implements OnInit {
  proposals: Proposal[]
  proposalsSubscription: Subscription

  constructor(private proposalService: ProposalService) { }

  ngOnInit() {
    this.getProposals()
  }

  getProposals() {
    this.proposalsSubscription = this.proposalService.proposals$.subscribe(
      (proposals: Proposal[]) => {
        this.proposals = proposals
    })

    this.proposalService.getProposals()
  }

}
