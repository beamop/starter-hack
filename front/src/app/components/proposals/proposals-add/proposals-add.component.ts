import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChallengeService } from 'src/app/services/challenge.service';
import { ChallengeCategory } from 'src/app/models/challenge.model';
import { Proposal } from 'src/app/models/proposal.model';
import { AuthService } from 'src/app/services/auth.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';

@Component({
  selector: 'app-proposals-add',
  templateUrl: './proposals-add.component.html',
  styleUrls: ['./proposals-add.component.scss']
})
export class ProposalsAddComponent implements OnInit {

  editor = ClassicEditor
  proposalForm: FormGroup
  categories: ChallengeCategory[]

  constructor(private challengeService: ChallengeService,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private swService: SweetAlertService) { }

  ngOnInit() {
    this.challengeService.getCategories().subscribe(
      (categories: ChallengeCategory[]) => this.categories = categories)
    this.initForm()
  }

  initForm() {
    this.proposalForm = this.formBuilder.group({
      title: ['', Validators.required],
      category: ['', Validators.required],
      level: ['', Validators.required],
      description: ['', Validators.required]
    })
  }

  onSubmit() {
    if (this.proposalForm.invalid) return false
    const proposal: Proposal = this.proposalForm.value
    proposal.user = this.authService.currentUserValue
    this.challengeService.sendProposal(this.proposalForm.value).subscribe(
      data => this.swService.sendAlert('Votre proposition a été envoyée !', 'success', 
      () => this.proposalForm.reset())
    )
  }

}
