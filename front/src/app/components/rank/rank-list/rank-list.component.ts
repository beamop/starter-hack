import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rank-list',
  templateUrl: './rank-list.component.html',
  styleUrls: ['./rank-list.component.scss']
})
export class RankListComponent implements OnInit {

  users: User[];
  usersSubscription: Subscription;

  constructor(private userService : UserService,
              private router : Router) { }

  ngOnInit() {
    this.usersSubscription = this.userService.users$.subscribe((users: User[]) => {
      this.users = users
    })
    this.userService.getUsersOrderedByPoints()
  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe()
  }

}
