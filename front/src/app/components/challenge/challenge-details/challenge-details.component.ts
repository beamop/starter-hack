import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Challenge } from 'src/app/models/challenge.model';
import { ChallengeService } from 'src/app/services/challenge.service';
import { ChallengeState, State } from 'src/app/models/challengeState.model';
import { Comment } from 'src/app/models/comment.model';
import { AuthService } from 'src/app/services/auth.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-challenge-details',
  templateUrl: './challenge-details.component.html',
  styleUrls: ['./challenge-details.component.scss']
})
export class ChallengeDetailsComponent implements OnInit {
  id: number
  challenge: Challenge;
  isStarting: boolean;
  challengeState: ChallengeState;
  hasResolved: boolean;
  timer: number;
  formRating: number = 1;
  comment: Comment
  editComment: Boolean = true;
  comments: Comment[] = []
  
  constructor(private route: ActivatedRoute,
              private challengeService: ChallengeService,
              private authService: AuthService,
              private swService: SweetAlertService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.getChallenge()
    this.getComments()
  }

  getComments(){
    this.challengeService.getComments(this.id).subscribe(comments => {
      this.comments = comments
      this.comment = comments.find(x => x.user.id === this.authService.currentUserValue.id);
      if(this.comment != null){
        this.editComment = false;
        this.formRating = this.comment.rating
      }
    })
  }

 rating(commentInput: string){
   if (commentInput.length == 0) return false
   this.comment = new Comment()
   this.comment.user = this.authService.currentUserValue
   this.comment.challenge = this.challenge
   this.comment.comment = commentInput
   this.comment.rating = this.formRating
   this.challengeService.comment(this.comment).subscribe(comment => {
      this.editComment = false
      this.getComments()
   })
 }

 deleteComment(){
   this.challengeService.deleteComment(this.challenge.id).subscribe(() => {
    this.getComments()
    this.editComment = true
    this.formRating = 1
  })
 }

  getState() {
    this.challengeService.getStartedChallenge(this.challenge.id).then(
      (state: ChallengeState) => {
        if (state) {
          this.timer = (state.challenge.timer-(new Date().getTime() - new Date(state.startDate).getTime())/1000)
          this.challengeState = state
          if (state.state == "STARTED") this.runTimer()
        }
      }
    )
  }

  getResolved() {
    this.challengeService.hasResolvedChallenge(this.challenge.id)
      .then(hasResolved => {
        this.hasResolved = hasResolved
      })
  }

  getChallenge() {
    const id = this.route.snapshot.params.id;
    this.challengeService.getChallenge(id)
      .subscribe(challenge => {
        if (challenge) {
          this.challenge = challenge;
          this.getValidationsCount()
          this.getState()
          this.getResolved()
        }
      },
      err => console.error(err))
  }

  getValidationsCount() {
    this.challengeService.getValidationsCount(this.challenge.id).then(
      (count: number) => this.challenge.validationsCount = count
    )
  }

  onStart() {
    this.isStarting = true
    this.challengeService.run(this.challenge.id).subscribe(
      (state: ChallengeState) => {
        this.challengeState = state;
        this.isStarting = false;
        this.runTimer()
        this.swService.sendAlert('Le challenge a démarré !', 'success')
      },
      err => console.error(err)
    )
  }

  onValidation(flag: string) {
    this.challengeService.checkFlag(flag, this.challengeState.id).subscribe(
      (challengeState: ChallengeState) => {
        this.challengeState = challengeState;
        if (challengeState.state == 'RESOLVED') {
          this.hasResolved = true;
          this.swService.sendAlert('Bonne réponse', 'success', () => {
            this.getChallenge()
            if (!this.hasResolved)
            this.setUserPoints(challengeState.user.points)
          })
        } else {
          this.swService.sendAlert('Mauvaise réponse !', 'error')
        }
      }
    )
  }

  setUserPoints(points: number) {
    const user = this.authService.currentUserValue;
    user.points = points;
    this.authService.currentUserValue = user;
  }

  runTimer() {
    this.timer = (this.challengeState.challenge.timer-(new Date().getTime() - new Date(this.challengeState.startDate).getTime())/1000)
    if (this.timer != 0) {
      const interval = setInterval(() => {
        this.timer --;
        if (this.timer == 0) 
        {
          this.challengeState.state = State.FAILED;
          clearInterval(interval)
        }
      },1000)
    }
  }
  
  get displayTime() {
    const minutes = Math.floor(this.timer/60);
    const displayMinutes = minutes > 9 ? minutes : `0${minutes}`

    const seconds = Math.floor(this.timer%60);
    const displaySeconds = seconds > 9 ? seconds : `0${seconds}`
    return `${displayMinutes}:${displaySeconds}`
  }

  get isStarted() {
    return !!this.challengeState && this.challengeState.state == State.STARTED
  }
}
