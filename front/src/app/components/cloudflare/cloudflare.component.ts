import { Component, OnInit } from '@angular/core';
import { CloudflareService } from 'src/app/services/cloudflare.service';

@Component({
  selector: 'app-cloudflare',
  templateUrl: './cloudflare.component.html',
  styleUrls: ['./cloudflare.component.scss']
})
export class CloudflareComponent implements OnInit {

  constructor(private cloudflareService: CloudflareService) { }

  ngOnInit() {
    this.cloudflareService.getDNSRecords().subscribe(data => console.log(data))
  }

}
