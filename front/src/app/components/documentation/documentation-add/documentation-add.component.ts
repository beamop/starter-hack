import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DocService } from 'src/app/services/doc.service';
import { Documentation } from 'src/app/models/documentation.model';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-documentation-add',
  templateUrl: './documentation-add.component.html',
  styleUrls: ['./documentation-add.component.scss']
})
export class DocumentationAddComponent implements OnInit {

  isEditing: boolean = false
  id: number
  documentation: Documentation

  constructor(private router: Router,
              private docService: DocService,
              private route: ActivatedRoute,) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.isEditing = true
      this.getDocumentationById(this.id)
    }
  }

  onSubmit() {
    this.router.navigate(["/documentations"])
  }

  getDocumentationById(id: Number) {
    this.docService.getDocById(id).then((documentation: Documentation) => {
      this.documentation = documentation
    })
  }

  onBack() {
    this.router.navigate(["/documentations"])
  }
}
