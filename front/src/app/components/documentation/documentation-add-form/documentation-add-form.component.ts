import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Documentation } from 'src/app/models/documentation.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DocService } from 'src/app/services/doc.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'doc-add-form',
  templateUrl: './documentation-add-form.component.html',
  styleUrls: ['./documentation-add-form.component.scss']
})
export class DocumentationAddFormComponent implements OnInit {

  @Input() doc: Documentation
  @Output() back = new EventEmitter()
  @Output() submit = new EventEmitter<Documentation>();
  docForm: FormGroup
  isEditing: boolean
  docType: string;
  fileToUpload: File;
  filename: string = "";
  fileURL: string;
  isUploading: boolean

  constructor(private formBuilder: FormBuilder,
    private docService: DocService,
    private swAlert: SweetAlertService) { }

  get isLink() {
    return this.docType == 'link'
  }

  get isFile() {
    return this.docType == 'file'
  }

  ngOnInit() {
    if (this.doc) {
      this.isEditing = true
      this.docType = this.docService.getDocType(this.doc)
      this.filename = this.doc.name
    } else {
      this.isEditing = false
      this.docType = "link"
    }
    this.initForm()
  }

  initForm() {
    this.docForm = this.formBuilder.group({
      name: [this.isEditing ? this.doc.name : "", [Validators.required]],
      url: this.isEditing ? this.doc.url : ""
    })
  }

  onSubmit() {
    if (!this.docForm.touched || !this.docForm.dirty) return false
    if (this.docForm.invalid) return false
    this.isUploading = true
    const doc = this.docForm.value
    if (this.doc) doc.id = this.doc.id
    

    const observable: Observable<Documentation> = this.isEditing ?
      this.docService.editDoc(doc) :
      this.docService.insertDoc(doc, this.fileToUpload)

    observable.subscribe((newDoc: Documentation) => {
      this.swAlert.sendAlert('Le document a été ' + (this.isEditing ? 'modifié !' : 'ajouté !'), 'success',
        () => {
          this.submit.next(newDoc);
          this.isUploading = false
        })
    })
  }

  handleFileInput(files: FileList) {
    if (files.item(0).type != 'application/pdf') return false
    this.fileToUpload = files.item(0);
    this.filename = this.fileToUpload.name
    this.readFile()
  }

  readFile() {
    var reader = new FileReader();
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = (_event: Event) => {
      this.fileURL = reader.result.toString();
    }
  }

  resetForm() {
    this.docForm.reset()
    this.fileURL = null
    this.fileToUpload = null
  }

}
