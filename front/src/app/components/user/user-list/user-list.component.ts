import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy{

  users: User[];
  usersSubscription: Subscription;

  constructor(private userService : UserService,
              private router: Router,
              private swService: SweetAlertService) { }

  ngOnInit() {
    this.usersSubscription = this.userService.users$.subscribe((users: User[]) => {
      this.users = users
    })
    this.userService.getUsers()
  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe()
  }

  onDelete(id: number){
    this.swService.displayConfirmation('Êtes-vous sûr de vouloir supprimer cet utilisateur ?', 'Cette action est irreversible', 
    'L\'utilisateur a bien été supprimé', () => {
      this.userService.deleteUser(id)
      this.router.navigate(['/user-list']);
    })
  }

  isAdmin(user: User) {
    this.userService.isUserAdmin(user)
  }
}
