import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  user: User;

  constructor(private userService: UserService,
              private route : ActivatedRoute,
              private authService : AuthService) { }


    get isAdmin() {
      return this.authService.isAdmin
    }

    get isCurrentUser() {
      return this.user.id == this.authService.currentUserValue.id
    }

  async ngOnInit() {
    const userId = this.route.snapshot.params.id
    if (userId) {
      this.user = await this.userService.getUserById(userId)
    } else this.user = this.authService.currentUserValue
  }


}
