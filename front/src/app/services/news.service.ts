import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { News } from '../models/news.model';
import { Subject } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  apiUrl = environment.apiUrl
  news$ = new Subject<News>()
  news: News

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  emitNews() {
    this.news$.next(this.news)
  }

  getNews() {
    return this.http.get(`${this.apiUrl}/news`)
      .subscribe((news: News) => {
        this.news = news
        this.news.items.map(n => n.safeContent = 
          this.sanitizer.bypassSecurityTrustHtml(n.content + '...'))
        this.emitNews()
      }, error => {
        console.error(error)
      })
  }
}
