import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { WebSocketService } from './websocket.service';
import { connect } from 'net';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  apiUrl = environment.apiUrl;
  users$ = new Subject<User[]>();
  users: User[] = [];

  constructor(private http: HttpClient,
              private authService: AuthService,
              private websocketService: WebSocketService) { 
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }
  
  emitUsers(){
    this.users$.next(this.users)
  }

  getUsers() {
    return this.http.get<User[]>(`${this.apiUrl}/users`).subscribe((users: User[]) => {
      this.users = users;
      this.emitUsers()
    })
  }

  getUsersOrderedByPoints(){
    return this.http.get<User[]>(`${this.apiUrl}/users/rank`).subscribe((users: User[]) => {
      this.users = users;
      this.emitUsers()
    })
  }

  getUserById(id: number): Promise<User> {
    return new Promise ((resolve, reject) => {
      this.http.get<User>(`${this.apiUrl}/users/${id}`).subscribe((user: User) => {
        resolve(user)
      }, err => reject(err))
    })
  }

  deleteUser(id: number){
    this.http.delete(`${this.apiUrl}/users/${id}`).subscribe(data => {
      const user = this.users.find(u => u.id == id)
      this.users.splice(this.users.indexOf(user), 1)
      this.emitUsers() 
    })

  }

  insertAdmin(user: User){
    return this.http.post<User>(`${this.apiUrl}/users/admin`, user).pipe(map((newUser: User) => {
      this.users.push(newUser)
      this.emitUsers()
      return newUser
    }))
  }

  updateUser(user: User) {
    const oldUser = this.users.find(u => u.id == user.id)
    return this.http.put<User>(`${this.apiUrl}/users`, user)
    .pipe(map((userRes: User) => {
      this.users[this.users.indexOf(oldUser)] = user
      if (this.authService.currentUserValue.id == user.id)
        this.authService.currentUserValue = userRes
        this.emitUsers()
        return userRes
    }))
  }

  updateAvatar(formData: FormData) {
    return this.http.put(`${this.apiUrl}/account/avatar`, formData)
  }

  isUserAdmin(user: User) {
    return (user.role == 'ADMIN' || user.role == 'SUPERADMIN')
  }

  searchUser(search: String): Promise<User[]>{
      return new Promise ((resolve, reject) => {
        this.http.get<User[]>(`${this.apiUrl}/search/user/${search}`).subscribe((users: User[]) => {
          resolve(users)
        }, err => reject(err))
      })
  }

}
