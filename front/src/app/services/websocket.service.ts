import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class WebSocketService {
    apiUrl = environment.apiUrl
    webSocketEndPoint: string = `${this.apiUrl}/ws`
    stompClient: Stomp
    message$ = new Subject()
    message

    constructor() { }

    emitMessage() {
        this.message$.next(this.message)
    }

    connect() {
        let ws = new SockJS(this.webSocketEndPoint)
        this.stompClient = Stomp.over(ws)
        this.stompClient.connect({}, (frame) => {
            this.stompClient.subscribe("/topic/starterhack", (event) => {
                this.onMessageReceived(event)
            })
        }, this.errorCallBack)
    }

    subscribeToTopic(topic: string, callback: CallableFunction) {
        let retryCount = 0

        if (this.stompClient.connected) {
            this.stompClient.subscribe("/topic/" + topic, (event) => {
                callback()
                this.onMessageReceived(event)
            })
        } else if (!(retryCount == 5)) {
            setTimeout(() => {
                this.subscribeToTopic(topic, callback)
                retryCount += 1
            }, 3000)
        }
    }

    disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect()
        }
        console.log("Disconnected")
    }

    errorCallBack(error) {
        console.log("errorCallBack -> " + error)
    }

    send(mapping, message) {
        this.stompClient.send("/app/" + mapping, {}, JSON.stringify(message));
    }

    onMessageReceived(message) {
        this.message = message
        this.emitMessage()
    }
}