import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Challenge, ChallengeCategory } from '../models/challenge.model';
import { map } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { ChallengeState, State } from '../models/challengeState.model';
import { resolve } from 'url';
import { Proposal } from '../models/proposal.model';
import { Comment } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class ChallengeService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getChallenges() {
    return this.http.get<Challenge[]>(`${this.apiUrl}/challenges/`)
      .pipe(map((response: Challenge[]) => {
        return response
      }, error => {
        console.error(error)
      }))
  }

  getComments(id) {
    return this.http.get<Comment[]>(`${this.apiUrl}/challenges/comments/${id}`)
      .pipe(map((response: Comment[]) => {
        return response
      }, error => {
        console.error(error)
      }))
  }

  getChallenge(id) {
    return this.http.get<Challenge>(`${this.apiUrl}/challenges/${id}`)
      .pipe(map((response: Challenge) => {
        return response
      }))
  }

  deleteChallenge(id) {
    return this.http.delete<any>(`${this.apiUrl}/challenges/${id}`)
  }
  deleteComment(id){
    return this.http.delete<any>(`${this.apiUrl}/challenges/comments/${id}`)
  }

  getCategories() {
    return this.http.get<ChallengeCategory[]>(`${this.apiUrl}/challenges/categories`)
  }

  addChallenge(formData) {
    return this.http.post(`${this.apiUrl}/challenges`, formData)
  }

  updateChallenge(formData) {
    return this.http.put(`${this.apiUrl}/challenges`, formData)
  }

  checkFlag(flag: string, challengeId: number): Observable<ChallengeState> {
    flag = encodeURIComponent(flag)
    return this.http.post<ChallengeState>(`${this.apiUrl}/challenges/resolve/${challengeId}?flag=${flag}`, null)
  }

  comment(formData) {
    return this.http.put<Comment>(`${this.apiUrl}/challenges/comment`, formData)
    .pipe(map((response: Comment) => {
      return response
    }))
  }

  run(challengeId: number) {
    return this.http.post(`${this.apiUrl}/challenges/run/${challengeId}`, null);
  }

  async getValidationsCount(challengeId: number) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.apiUrl}/challenges/validationsCount/${challengeId}`)
        .subscribe(
          (challenge: Challenge) => resolve(challenge),
          err => reject(err))
    })
  }

  async getResolvedChallenges() {
    return new Promise((resolve, reject) => {
      this.http.get<Challenge[]>(`${this.apiUrl}/challenges/resolved`).subscribe(
        (challenges: Challenge[]) => resolve(challenges)
      ),
        err => reject(err)
    })
  }

  async hasResolvedChallenge(challengeId: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.get<boolean>(`${this.apiUrl}/challenges/resolved/${challengeId}`).subscribe(
        (hasResolved: boolean) => resolve(hasResolved)
      ),
        err => reject(err)
    })
  }

  async getStartedChallenge(challengeId: number) {
    return new Promise((resolve, reject) => {
      this.http.get<ChallengeState>(`${this.apiUrl}/challenges/started/${challengeId}`).subscribe(
        (state: ChallengeState) => resolve(state)
      ),
        err => reject(err)
    })
  }

  uploadScript(formData: FormData) {
    return this.http.post(`${this.apiUrl}/challenges/script`, formData, { responseType: 'text' })
  }

  getChallengesByCategory(categoryName: string) {
    return this.http.get<Challenge[]>(`${this.apiUrl}/challenges/category/${categoryName}`)
  }
  
  async getScripts() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.apiUrl}/challenges/scripts`).subscribe(
        (scriptsList) => {
          resolve(scriptsList)
        }
      ),
      err => reject(err)
    })
  }

  sendProposal(proposal: Proposal) {
    return this.http.post(`${this.apiUrl}/proposals`, proposal)
  }

  searchChallenge(search: String): Promise<Challenge[]>{
    return new Promise ((resolve, reject) => {
      this.http.get<Challenge[]>(`${this.apiUrl}/search/challenge/${search}`).subscribe((challenge: Challenge[]) => {
        resolve(challenge)
      }, err => reject(err))
    })
}
}